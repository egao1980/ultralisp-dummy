(defpackage ultralisp-dummy/tests/main
  (:use :cl
        :ultralisp-dummy
        :rove))
(in-package :ultralisp-dummy/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :ultralisp-dummy)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
