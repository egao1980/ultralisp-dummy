(defsystem "ultralisp-dummy"
  :version "0.1.0"
  :author "Nikolai Matiushev"
  :license "BSD"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "Dummy project for Ultralisp integration development"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "ultralisp-dummy/tests"))))

(defsystem "ultralisp-dummy/tests"
  :author "Nikolai Matiushev"
  :license "BSD"
  :depends-on ("ultralisp-dummy"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for ultralisp-dummy"

  :perform (test-op (op c) (symbol-call :rove :run c)))
